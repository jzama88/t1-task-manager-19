package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {


    @Override
    public Task create(String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(String name, String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId){
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
        if (task.getProjectId() == null) continue;
        if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
