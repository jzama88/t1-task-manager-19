package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository <M extends AbstractModel> implements IRepository<M>
{
    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(Sort sort) {
       final Comparator<M> comparator = sort.getComparator();
       return findAll(comparator);
    }

    @Override
    public M findOneById(String id) {
        for (final M model: records){
            if(id.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(Integer index) {
        return records.get(index);
    }

    @Override
    public M remove(M model) {
       records.remove(model);
       return model;
    }

    @Override
    public M removeById(String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public void clear()
    {
        records.clear();
    }

    @Override
    public int getSize() {
        return records.size();
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }
}
