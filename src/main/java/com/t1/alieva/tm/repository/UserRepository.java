package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {


    @Override
    public User create(String login, String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        final User user = create (login,password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        final User user = create (login,password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User findOneByLogin(String login) {
        for(final User user: records)
        {
            if(login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findOneByEmail(String email) {
        for(final User user: records)
        {
            if(email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        for(final User user: records)
        {
            if(login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        for(final User user: records)
        {
            if(email.equals(user.getEmail())) return true;
        }
        return false;
    }
}
