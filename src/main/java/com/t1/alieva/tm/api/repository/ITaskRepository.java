package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task create(String name);

    Task create(String name,String description);

    List<Task> findAllByProjectId(String projectId);

}
