package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task create(String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task create(String name, String description, Date dateBegin, Date dateEnd) throws AbstractEntityNotFoundException, AbstractFieldException;

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task changeTaskStatusById(String id, Status status) throws AbstractFieldException;



}
