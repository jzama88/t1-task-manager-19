package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.service.ProjectService;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService extends IService<Project>{

    Project create (String name) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project create (String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project create (String name, String description, Date dateBegin, Date dateEnd) throws AbstractEntityNotFoundException, AbstractFieldException;

    Project updateById(String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project changeProjectStatusById(String id, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

}
