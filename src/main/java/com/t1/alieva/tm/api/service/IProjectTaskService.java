package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId) throws AbstractFieldException, AbstractEntityNotFoundException;

    void removeProjectById(String projectId) throws AbstractFieldException, AbstractEntityNotFoundException;

    void unbindTaskFromProject(String projectId, String taskId) throws AbstractFieldException, AbstractEntityNotFoundException;

}
