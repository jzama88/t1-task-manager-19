package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import com.t1.alieva.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email) throws AbstractUserException, AbstractFieldException, AbstractEntityNotFoundException;

    void login(String login, String password) throws AbstractFieldException, AccessDeniedException, AbstractUserException;

    void logout();

    boolean isAuth();

    String getUserId() throws AbstractUserException;

    User getUser() throws AbstractUserException, AbstractFieldException;

}
