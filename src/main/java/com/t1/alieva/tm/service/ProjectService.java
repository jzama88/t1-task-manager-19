package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.service.IProjectService;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.ProjectNotFoundException;
import com.t1.alieva.tm.exception.field.*;
import com.t1.alieva.tm.model.Project;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository projectRepository) {

        super(projectRepository);
    }

    @Override
    public Project create(final String name) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        if(description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name,description);
    }

    @Override
    public Project create(
            final String name,
            final String description,
            final Date dateBegin,
            final Date dateEnd) throws AbstractEntityNotFoundException, AbstractFieldException {
        final Project project = create(name,description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
     public Project updateById(final String id, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException{
        if(id == null || id.isEmpty()) throw new IdEmptyException();
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if(project == null) throw new ProjectNotFoundException() ;
        project.setName(name);
        project.setDescription(description);
        return project;
     }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(index == null || index < 0) throw new IndexIncorrectException();
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if(project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }
}
