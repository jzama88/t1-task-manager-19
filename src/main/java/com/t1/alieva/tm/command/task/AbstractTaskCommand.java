package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.api.service.IProjectTaskService;
import com.t1.alieva.tm.api.service.ITaskService;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {


    protected ITaskService getTaskService(){
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService(){
        return getServiceLocator().getProjectTaskService();
    }

    protected void renderTasks(final List<Task> tasks ){
        int index = 1;
        for (final Task task : tasks) {
            if(task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(final Task task){
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }
}
