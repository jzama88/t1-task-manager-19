package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.service.ICommandService;
import com.t1.alieva.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService(){
        return serviceLocator.getCommandService();
    }
}
