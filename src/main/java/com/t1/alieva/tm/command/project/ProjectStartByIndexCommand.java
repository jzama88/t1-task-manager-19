package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "p-start-by-index";
    }

    @Override
    public String getDescription() {
        return "Start Project by Index.";
    }

    @Override
    public void execute() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        getProjectService().changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }
}
