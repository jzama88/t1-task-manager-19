package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IProjectTaskService;
import com.t1.alieva.tm.api.service.ITaskService;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public void showUser(User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    public String getArgument() {
        return null;
    }
}
