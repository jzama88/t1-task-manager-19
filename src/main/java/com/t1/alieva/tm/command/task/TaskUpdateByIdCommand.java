package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "t-update-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update Task by ID.";
    }

    @Override
    public void execute() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println(("[UPDATE TASK BY ID]"));
        System.out.println(("[ENTER ID]"));
        final String id = TerminalUtil.nextLine();
        System.out.println(("[ENTER NAME]"));
        final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        final String description = TerminalUtil.nextLine();
        getTaskService().updateById(id, name, description);
    }
}
