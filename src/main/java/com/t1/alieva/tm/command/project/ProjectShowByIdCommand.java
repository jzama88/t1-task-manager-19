package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand{

    @Override
    public String getName() {
        return "p-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Show Project by ID.";
    }

    @Override
    public void execute() throws AbstractFieldException {
        System.out.println(("[SHOW PROJECT BY ID]"));
        System.out.println(("[ENTER ID]"));
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        showProject(project);
    }
}
