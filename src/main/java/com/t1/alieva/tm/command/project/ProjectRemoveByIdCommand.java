package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "p-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Remove Project by ID.";
    }

    @Override
    public void execute() throws AbstractFieldException, AbstractEntityNotFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().removeById(id);
        getProjectTaskService().removeProjectById(project.getId());
    }
}
