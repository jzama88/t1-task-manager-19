package com.t1.alieva.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException{

    public IndexIncorrectException(){
        super("Error! Index is incorrect...");
    }
}
